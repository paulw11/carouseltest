//
//  WordView.swift
//  CarouselTest
//
//  Created by Paul Wilkinson on 14/04/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import Foundation
import UIKit

class WordView : UIView {
    
    var wordLabel:UILabel
    var smallWordLabel:UILabel
    
    var bigColor=UIColor.whiteColor()
    var smallColor=UIColor.blackColor()
    
    var word : String {
        set(newValue) {
            self.wordLabel.text=newValue
            self.smallWordLabel.text=newValue
        }
        get {
            return self.wordLabel.text!
        }
    }
    
    override init(frame: CGRect) {
        
        self.wordLabel=UILabel()
        self.smallWordLabel=UILabel()
        
        super.init(frame: frame)
        
        self.wordLabel.translatesAutoresizingMaskIntoConstraints=false
        
        self.addSubview(self.wordLabel)
        
        self.wordLabel.textAlignment=NSTextAlignment.Center
        self.wordLabel.font=UIFont.systemFontOfSize(28)
        self.wordLabel.textColor=self.bigColor
        
        self.addConstraint(NSLayoutConstraint(item: self.wordLabel, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: self.wordLabel, attribute: .Trailing, relatedBy: .Equal, toItem: self, attribute: .Trailing, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: self.wordLabel, attribute: .Top , relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: self.wordLabel, attribute: .Bottom, relatedBy: .Equal, toItem: self, attribute: .Bottom, multiplier: 1, constant: 0))
        
        self.smallWordLabel.translatesAutoresizingMaskIntoConstraints=false
        
        self.addSubview(self.smallWordLabel)
        
        self.smallWordLabel.textAlignment=NSTextAlignment.Center
        
        self.smallWordLabel.alpha=0
        self.smallWordLabel.font=UIFont.systemFontOfSize(22)
        self.smallWordLabel.textColor=self.smallColor
        
        self.addConstraint(NSLayoutConstraint(item: self.smallWordLabel, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: self.smallWordLabel, attribute: .Trailing, relatedBy: .Equal, toItem: self, attribute: .Trailing, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: self.smallWordLabel, attribute: .Top , relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: self.smallWordLabel, attribute: .Bottom, relatedBy: .Equal, toItem: self, attribute: .Bottom, multiplier: 1, constant: 0))
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func embiggen() {
        self.smallWordLabel.textColor=self.bigColor
        self.smallWordLabel.alpha=0.0
        self.wordLabel.textColor=self.bigColor
        self.wordLabel.alpha=1.0
    }
    
    func ensmallen() {
        self.smallWordLabel.textColor=self.smallColor
        self.smallWordLabel.alpha=1.0
        self.wordLabel.textColor=self.smallColor
        self.wordLabel.alpha=0.0
    }
}