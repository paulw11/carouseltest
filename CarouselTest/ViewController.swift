//
//  ViewController.swift
//  CarouselTest
//
//  Created by Paul Wilkinson on 14/04/2016.
//  Copyright © 2016 Paul Wilkinson. All rights reserved.
//

import UIKit
import iCarousel

class ViewController: UIViewController,  iCarouselDataSource, iCarouselDelegate {
    
    @IBOutlet weak var carousel: iCarousel!
    
    let words=["Dog","Cat","Pig","Cow","Chicken","Fish"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        carousel.centerItemWhenSelected=true
        carousel.stopAtItemBoundary=true
        carousel.scrollToItemBoundary=true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return words.count;
    }
    
    func carouselItemWidth(carousel: iCarousel) -> CGFloat {
        return carousel.frame.width/3
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        let height = carousel.frame.size.height
        let width = carousel.frame.size.width/3
        let cviewFrame=CGRect(x: 0, y: 0, width: width, height: height)
        let cview=WordView(frame:cviewFrame)
        
        
        cview.word=self.words[index]
        
        self.setViewLabelAppearance(cview, forIndex: index)
        
        return cview
    }
    
    func carouselCurrentItemIndexDidChange(carousel: iCarousel) {
        let cview = carousel.currentItemView as! WordView
        self.setViewLabelAppearance(cview, forIndex: carousel.currentItemIndex)
        
        var beforeIndex=carousel.currentItemIndex-1
        
        if (beforeIndex < 0) {
            beforeIndex = self.words.count-1
        }
        
        let beforeView=carousel.itemViewAtIndex(beforeIndex) as! WordView
        
        self.setViewLabelAppearance(beforeView, forIndex: beforeIndex)
        
        var afterIndex=carousel.currentItemIndex+1
        
        if (afterIndex == self.words.count) {
            afterIndex = 0
        }
        
        let afterView=carousel.itemViewAtIndex(afterIndex) as! WordView
        
        self.setViewLabelAppearance(afterView, forIndex: afterIndex)
        
        
    }
    
    func setViewLabelAppearance(cview:WordView, forIndex index:Int) {
        
        var embiggen=false
        
        
        if (self.carousel.currentItemIndex == index) {
            embiggen=true
        }
        
            if (embiggen) {
                cview.embiggen()
            } else {
                cview.ensmallen()
        }
    }
    
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.Wrap {
            return 1.0
        } else {
            return value
        }
    }
    


}

